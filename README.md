## LTE Scheduling Data - Cleaning and Analysis
#### Author: Marcus Fong | Date last edited: 29th Feb 2020

In this project, I investigate how 4G LTE networks (particularly the eNodeB) 
allocates resource blocks when it has multiple phones connected to it. 

I used open source software, srsLTE, to emulated a 4G network using software
defined radio and virtual machines. I then captured packets entering being
send via the LTE air interface on the device side which you can see in
"ue-export2.txt". 

The data I extracted was quite messy so I cleaned it up using the Clean.py
script and saved the cleaned data in "output2.csv".

Finally, I am doing data visualization of the data I cleaned in Visualise.py.
At the moment, i'm troubleshooting some errors in the Visualise.py and hope
to have it up and running soon.