# -*- coding: utf-8 -*-
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import csv
from idlelib.search import find
import matplotlib.patches as mpatches
from matplotlib.font_manager import FontProperties

with open('output2.csv') as f:
    RbList = [tuple(line) for line in csv.reader(f)]

# data
del RbList[0]

print(RbList)

RbList1 = [
    # timestamp, direction, rb start, rb length, channel, rnti
    ('57316820', '1', '44', '4', 'PUSCH', '127'),
    ('57316820', '1', '2', '15', 'PUSCH', '130'),
    ('57316821', '1', '29', '15', 'PUSCH', '129'),
    ('57316821', '1', '2', '27', 'PUSCH', '131'),
    ('57316947', '1', '34', '3', 'PUSCH', '132'),
    ('57316955', '1', '34', '3', 'PUSCH', '132'),
    ('57316963', '1', '34', '3', 'PUSCH', '132'),
    ('57317000', '1', '2', '15', 'PUSCH', '127'),
    ('57317000', '1', '17', '27', 'PUSCH', '129'),
    ('57317000', '1', '44', '4', 'PUSCH', '130'),
    ('57317001', '1', '2', '27', 'PUSCH', '131'),
    ('57317020', '1', '29', '15', 'PUSCH', '132'),
    ('57317020', '1', '44', '4', 'PUSCH', '145'),
    ('57317021', '1', '2', '27', 'PUSCH', '146'),
    ('57317180', '1', '2', '15', 'PUSCH', '127'),
    ('57317180', '1', '17', '15', 'PUSCH', '129')
]


# print(RbList)

def time(i):
    return int(RbList[i][0][:-1])  # - 57316820

#Resource block start :: int
def rs(i):
    if RbList[i][2] == ' ':
        return 0
    else:
        return int(RbList[i][2])

#Resource block length :: int
def rl(i):
    if RbList[i][3] == ' ':
        return 0
    else:
        return int(RbList[i][3])


def color(i):
    if int(RbList[i][5]) == 127:
        return '#ff0000'
    elif int(RbList[i][5]) == 129:
        return '#ff8300'
    elif int(RbList[i][5]) == 130:
        return '#ffeb00'
    elif int(RbList[i][5]) == 131:
        return '#94ff00'
    elif int(RbList[i][5]) == 132:
        return '#00ff39'
    elif int(RbList[i][5]) == 145:
        return '#00e6ff'
    elif int(RbList[i][5]) == 146:
        return '#0044ff'
    elif int(RbList[i][5]) == 147:
        return '#ec00ff'
    else:
        return '#ff00c1'


def rbPlot(i):
    return matplotlib.patches.Rectangle((time(i) - time(0), rs(i)), 1, rl(i), color=color(i))


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_ylabel('resource block')
ax.set_xlabel('time')

legend = [('#ff0000', 'rnti127'),
          ('#ff8300', 'rnti129'),
          ('#ffeb00', 'rnti130'),
          ('#94ff00', 'rnti131'),
          ('#00ff39', 'rnti132'),
          ('#00e6ff', 'rnti145'),
          ('#0044ff', 'rnti146'),
          ('#ec00ff', 'rnti147'),
          ('#ff00c1', 'rnti002')]



def patch(i):
    return mpatches.Patch(color=legend[i][0], label=legend[i][1])


plt.legend(handles=[patch(0), patch(1), patch(2), patch(3), patch(4), patch(5), patch(6), patch(7), patch(8)],
           loc="upper left", bbox_to_anchor=(1.0, 0.6))

for i in range(len(RbList)):
    rbPlot(i)
    ax.add_patch(rbPlot(i))

plt.grid()
plt.xlim([-5, 1300])
plt.ylim([-5, 50])
plt.style.use('dark_background')
plt.show()