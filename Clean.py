import csv
from idlelib.search import find

# Attributes to keep: timestamp, dir, rb_start, l_crb, channel, rnti
# Note: header needs to be removed for cleaning to operate successfully
# Import text document

doc = 'ue-export2.txt'

with open(doc) as f:
    content = f.readlines()
content = [x.strip() for x in content]

# Set attribute to find

attrList = ['timestamp\":', 'dir\":', 'rb_start\":', 'l_crb\":', 'channel\":\"', 'rnti\":']
attribute = attrList[0]


# Find position of word in certain line
def position(line, word):
    return line.find(word)


# Position of beginning of attribute , n denotes attr number
def beg(i, n):
    return position(content[i], attrList[n]) + len(attrList[n])


# Position of ending of attribute's data
def end(i, n):
    for x in range(30):
        if content[i][beg(i, n) + x:beg(i, n) + x + 1] == ',' or content[i][beg(i, n) + x: beg(i, n) + x + 1] == '\"':
            return beg(i, n) + x


def row(i):
    return value(i, 0), value(i, 1), value(i, 2), value(i, 3), value(i, 4), value(i, 5)


# y denotes row number and n denotes attribute
def value(y, n):
    if content[y][beg(y, n):end(y, n)][0][0] == '0' or content[y][beg(y, n):end(y, n)][0][0] == '1' or \
            content[y][beg(y, n):end(y, n)][0][0] == '2' or content[y][beg(y, n):end(y, n)][0][0] == '3' or \
            content[y][beg(y, n):end(y, n)][0][0] == '4' or content[y][beg(y, n):end(y, n)][0][0] == '5' or \
            content[y][beg(y, n):end(y, n)][0][0] == '6' or content[y][beg(y, n):end(y, n)][0][0] == '7' or \
            content[y][beg(y, n):end(y, n)][0][0] == '8' or content[y][beg(y, n):end(y, n)][0][0] == '9' or \
            content[y][beg(y, n):end(y, n)][0][0] == 'P':
        return content[y][beg(y, n):end(y, n)]
    else:
        return " "


def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
        return i + 1


header = ("Timestamp", "Direction", "Resource Block Start", "Resource Block Length", "Channel", "RNTI")
#
print(header)
for x in range(file_len(doc)):
    print(row(x))


with open('output2.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(header)
    for x in range(file_len(doc)):
        writer.writerow(row(x))
